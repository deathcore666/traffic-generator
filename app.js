const Gen = require('./src/controller/generateCustomRangeData');

// let daysCounter = 1;
// let aJob;
// let inserter;
// const params = {
//     "user": "beeline_admin",
//     "password": "beeline",
//     "connectString": "192.168.0.166:32769/ORCLCDB.localdomain"
// };
//
// const tableName = 'USAGE_1';
//
// const conn = async () => {
//     inserter = new DataInsert(parameters, tableName);
//     await inserter.connect();
// };
//
// const main = async () => {
//     aJob = new Cloner(daysCounter++, inserter);
//
//     aJob.on('end', () => {
//         main();
//     });
//
//     aJob.on('error', (err) => {
//        console.error('Error executing operation:', err);
//         shutdown();
//     });
//     const conn = async () => {
//         await aJob.connect();
//     };
//     conn()
//         .then(()=> aJob.clone())
//         .catch(err => console.log('Error connecting:', err));
// };
//
// const shutdown = () => {
//     aJob.releaseConnection();
// };
//
// conn().then(()=>main());



const main = () => {
    let gen = new Gen();
    gen.connect()
        .then(() => gen.start())
        .catch(err=>console.log('error:', err));
};

main();