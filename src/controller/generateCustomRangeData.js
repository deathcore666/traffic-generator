const moment = require('moment');

const {Producer} = require('rib-kafka');
const DataInsert = require('../oracle/dataInsert');
const Parameters = require('../../config/config');

const kafka = Parameters.kafka;
const oralceParams = Parameters.oralceParams;

class CustomFakeDataGenerator {
    constructor() {
        this.parameters = CustomFakeDataGenerator.validateParams(Parameters.genParams);
        this.dataInsertInstance = new DataInsert(oralceParams, oralceParams.tableName);
        this.producer = new Producer({config: kafka});
    }

    async connect() {
        await this.dataInsertInstance.connect();
        this.producer.on('error', err => console.log('producer error:', err));
        await this.producer.connect();
    }

    static validateParams(params) {
        let startDate = params.datesRange[0] ? moment(params.datesRange[0]) : moment('2018-01-01');
        let endDate = params.datesRange[1];
        if (!endDate)
            endDate = moment(startDate).add(1, 'year');

        const AVERAGE_CALLS_PER_DAY = Parameters.genParams.AVERAGE_CALLS_PER_DAY || 5;
        const AVERAGE_DATA_VOL_PER_DAY = Parameters.genParams.AVERAGE_DATA_VOL_PER_DAY || 256;
        const AVERAGE_SMS_PER_DAY = Parameters.genParams.AVERAGE_SMS_PER_DAY || 4;
        const AVERAGE_CALL_DURATION_SEC = Parameters.genParams.AVERAGE_CALL_DURATION_SEC || 60;
        const SMS_PRICE = Parameters.genParams.SMS_PRICE || 2;
        const DATA_PRICE = Parameters.genParams.DATA_PRICE || 5;
        const CALL_PRICE = Parameters.genParams.CALL_PRICE || 0.05;

        let datesDiff = endDate.diff(startDate, 'days');
        let callsCount = params.callsCount || datesDiff * AVERAGE_CALLS_PER_DAY;
        let callsDuration = params.callsDuration || callsCount * AVERAGE_CALL_DURATION_SEC;
        let chargedCallsDuration = params.chargedCallsDuration || Math.ceil(callsDuration / 3 * 2);
        let callsCharge = chargedCallsDuration * CALL_PRICE;
        let smsCount = params.smsCount || datesDiff * AVERAGE_SMS_PER_DAY;
        let chargedSmsCount = params.chargedSmsCount || Math.ceil(smsCount / 3 * 2);
        let smsCharge = chargedSmsCount * SMS_PRICE;
        let dataVol = params.dataVol || datesDiff * AVERAGE_DATA_VOL_PER_DAY;
        let chargedDataVol = params.dataVol || Math.ceil(dataVol / 3 * 2);
        let dataCharge = chargedDataVol * DATA_PRICE;

        return {
            msisdnRange: params.msisdnRange,
            startDate: startDate,
            endDate: endDate,
            datesDiff: datesDiff,
            callsCount: callsCount,
            callsDuration: callsDuration,
            chargedCallsDuration: chargedCallsDuration,
            callsCharge: callsCharge,
            smsCount: smsCount,
            chargedSmsCount: chargedSmsCount,
            smsCharge: smsCharge,
            dataVol: dataVol,
            chargedDataVol: chargedDataVol,
            dataCharge: dataCharge,
            changePhone: params.changePhone,
            // SMS_PRICE           : SMS_PRICE,
            // CALL_PRICE          : CALL_PRICE,
            // DATA_PRICE          : DATA_PRICE
        }
    }

    async start() {
        let lastNumberInRange = parseInt(this.parameters.msisdnRange[1]);
        let firstNumberInRange = parseInt(this.parameters.msisdnRange[0]);

        for (let date = 0; date < this.parameters.datesDiff; date++) {
            for (let i = firstNumberInRange; i <= lastNumberInRange; i++) {
                let dt = moment(this.parameters.startDate).add(date, 'day');
                await this.generateOneDayCallsForOneMsisdn(i, dt);
                await this.generateOneDayDataForOneMsisdn(i, dt);
                await this.generateOneDaySmsForOneMsisdn(i, dt);
            }
        }
    }

    async generateOneDayDataForOneMsisdn(msisdn, date) {
        let connectionTimes = this.parameters.dataVol / this.parameters.datesDiff;
        let dataPerConnection = this.parameters.dataVol / connectionTimes;
        let freeDataVol = this.parameters.dataVol - this.parameters.chargedDataVol;

        let freeDataConnections = freeDataVol / dataPerConnection;
        let chargedDataConnections = this.parameters.chargedDataVol / dataPerConnection;
        let dataPrice = this.parameters.chargedDataVol / this.parameters.dataCharge;

        const freeData = () => {
            return new Promise(async (resolve, reject) => {
                for (let i = 0; i < freeDataConnections; i++) {
                    let record = {
                        PHONE_NUM: msisdn + '',
                        START_DT: new Date(date),
                        END_DT: new Date(date),
                        CALL_TYPE_KEY: 1205,
                        COUNTRY_KEY: 895,
                        REGION_KEY: '',
                        CITY_KEY: 22829,
                        IMEI_CVAL: '',
                        B_PHONE_NUM: '',
                        B_COUNTRY_KEY: 895,
                        B_CITY_KEY: '',
                        B_REGION_KEY: '',
                        DURATION_NVAL: 0,
                        CHARGED_DURATION_NVAL: 0,
                        DATA_VOL_NVAL: dataPerConnection,
                        CHARGED_DATA_VOL_NVAL: 0,
                        CHARGE_NVAL: 0,
                    };
                    // this.producer.produce('beeline.internet.logs', null, new Buffer(JSON.stringify(record)), null, Date.now())
                    //     .catch(err => console.log('Error at data producer:', err));
                    try {
                        await this.dataInsertInstance.insertRecord(record);
                    } catch (e) {
                        reject(e);
                    }
                    resolve({});
                }
            })
        };
        const chargedData = () => {
            return new Promise(async (resolve, reject) => {
                for (let i = 0; i < chargedDataConnections; i++) {
                    let record = {
                        PHONE_NUM: msisdn + '',
                        START_DT: new Date(date),
                        END_DT: new Date(date),
                        CALL_TYPE_KEY: 1205,
                        COUNTRY_KEY: 895,
                        REGION_KEY: '',
                        CITY_KEY: 22829,
                        IMEI_CVAL: '',
                        B_PHONE_NUM: '',
                        B_COUNTRY_KEY: 895,
                        B_CITY_KEY: '',
                        B_REGION_KEY: '',
                        DURATION_NVAL: 0,
                        CHARGED_DURATION_NVAL: 0,
                        DATA_VOL_NVAL: dataPerConnection,
                        CHARGED_DATA_VOL_NVAL: dataPerConnection,
                        CHARGE_NVAL: dataPerConnection * dataPrice,
                    };
                    // this.producer.produce('beeline.internet.logs', null, new Buffer(JSON.stringify(record)), null, Date.now())
                    //     .catch(err => console.log('Error at data producer:', err));
                    try {
                        await this.dataInsertInstance.insertRecord(record);
                    } catch (e) {
                        reject(e);
                    }
                    resolve({});
                }
            })
        };

        await freeData();
        await chargedData();
    }


    async generateOneDaySmsForOneMsisdn(msisdn, date) {
        let requiredCount = this.parameters.callsCount / this.parameters.datesDiff;
        for (let i = 0; i < requiredCount; i++) {
            let sms = {
                PHONE_NUM: msisdn + '',
                START_DT: new Date(date),
                END_DT: new Date(date),
                CALL_TYPE_KEY: 1191,
                COUNTRY_KEY: 895,
                REGION_KEY: 0,
                CITY_KEY: 22829,
                IMEI_CVAL: 0,
                B_PHONE_NUM: '',
                B_COUNTRY_KEY: 895,
                B_CITY_KEY: 0,
                B_REGION_KEY: 0,
                DURATION_NVAL: 0,
                CHARGED_DURATION_NVAL: 0,
                DATA_VOL_NVAL: 0,
                CHARGED_DATA_VOL_NVAL: 0,
                CHARGE_NVAL: Math.ceil(this.parameters.smsCharge / this.parameters.smsCount),
            };

            await
                this.dataInsertInstance.insertRecord(sms)
            // this.producer.produce('beeline.sms', null, new Buffer(JSON.stringify(sms)), null, Date.now())
            //     .catch(err => console.log('Error at sms producer:', err));
        }
    }

    async generateOneDayCallsForOneMsisdn(msisdn, date) {
        let currDate = date;

        let durationPerCall = this.parameters.callsDuration / this.parameters.callsCount;
        let callPricePerSecond = this.parameters.callsCharge / this.parameters.chargedCallsDuration;
        let requiredCountPerDay = this.parameters.callsCount / this.parameters.datesDiff;

        for (let i = 0; i < requiredCountPerDay; i++) {
            let dt = moment(currDate).add(durationPerCall, 'second');
            let call = {
                PHONE_NUM: msisdn + '',
                START_DT: new Date(currDate),
                END_DT: new Date(dt),
                CALL_TYPE_KEY: 1160,
                COUNTRY_KEY: 895,
                REGION_KEY: 0,
                CITY_KEY: 22829,
                IMEI_CVAL: '',
                B_PHONE_NUM: '',
                B_COUNTRY_KEY: 895,
                B_CITY_KEY: 0,
                B_REGION_KEY: 0,
                DURATION_NVAL: durationPerCall,
                CHARGED_DURATION_NVAL: durationPerCall,
                DATA_VOL_NVAL: 0,
                CHARGED_DATA_VOL_NVAL: 0,
                CHARGE_NVAL: Math.round(durationPerCall * callPricePerSecond * 100) / 100,
            };
            currDate = currDate.add(durationPerCall, 'second');

            await
                this.dataInsertInstance.insertRecord(call)
            // this.producer.produce('beeline.calls', null, new Buffer(JSON.stringify(call)), null, Date.now())
            //     .catch(err => console.log('Error at calls producer:', err));

        }
    }
}

module.exports = CustomFakeDataGenerator;
