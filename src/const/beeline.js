const CALLS                         = 'beeline.calls';
const SMS                           = 'beeline.sms';
const INTERNET_LOGS                 = 'beeline.internetLogs';
const TARIFICATION                  = 'beeline.tarification';
const ROAMING_CALLS                 = 'beeline.roaming.calls';
const ROAMING_SMS                   = 'beeline.roaming.sms';
const ROAMING_INTERNET_LOGS         = 'beeline.roaming.internetLogs';
const ROAMING_TARIFICATION          = 'beeline.roaming.tarification';
const HISTORY                       = 'beeline.history';
const LOCATION                      = 'beeline.location';
const BALANCE_REMAINING             = 'beeline.balance.remaining';
const BALANCE_CREDIT                = 'beeline.balance.credit';
const BALANCE_TOPUP                 = 'beeline.balance.topUp';

const USAGE_QUERY                   = 'SELECT * FROM usage';
const BALANCE_REMAINING_QUERY       = 'SELECT * FROM balance';
const BALANCE_TOPUP_QUERY           = 'SELECT * FROM payment';
const BALANCE_CREDIT_QUERY          = 'SELECT * FROM trustpayment';

const START_DT                      = 'START_DT';
const END_DT                        = 'END_DT';
const EVENT_DT                      = 'EVENT_DT';
const B_COUNTRY_KEY                 = 'B_COUNTRY_KEY';
const COUNTRY_KEY                   = 'COUNTRY_KEY';
const CHARGE_NVAL                   = 'CHARGE_NVAL';
const PHONE_NUM                     = 'PHONE_NUM';
const B_PHONE_NUM                   = 'B_PHONE_NUM';
const CELL_NVAL                     = 'CELL_NVAL';
const IMEI_CVAL                     = 'IMEI_CVAL';
const SUBS_KEY                      = 'SUBS_KEY';
const CALL_TYPE_KEY                 = 'CALL_TYPE_KEY';
const DURATION_NVAL                 = 'DURATION_NVAL';
const DATA_VOL_NVAL                 = 'DATA_VOL_NVAL';
const PRICE_PLAN_KEY                = 'PRICE_PLAN_KEY';
const RECHARGE_VAL                  = 'RECHARGE_VAL';
const BALANCE_NVAL                  = 'BALANCE_NVAL';

const USAGE_TABLE                   = 'usage';
const BALANCE_REMAINING_TABLE       = 'balance';
const BALANCE_TOPUP_TABLE           = 'payment';
const BALANCE_CREDIT_TABLE          = 'trustpayment';


module.exports = {
    CALLS_TOPIC: CALLS,
    SMS_TOPIC: SMS,
    INTERNET_LOGS_TOPIC: INTERNET_LOGS,
    TARIFICATION_TOPIC: TARIFICATION,
    ROAMING_CALLS_TOPIC: ROAMING_CALLS,
    ROAMING_SMS_TOPIC: ROAMING_SMS,
    ROAMING_INTERNET_LOGS_TOPIC: ROAMING_INTERNET_LOGS,
    ROAMING_TARIFICATION_TOPIC: ROAMING_TARIFICATION,
    HISTORY_TOPIC: HISTORY,
    LOCATION_TOPIC: LOCATION,
    BALANCE_REMAINING_TOPIC: BALANCE_REMAINING,
    BALANCE_CREDIT_TOPIC: BALANCE_CREDIT,
    BALANCE_TOPUP_TOPIC: BALANCE_TOPUP,

    USAGE_QUERY,
    BALANCE_TOPUP_QUERY,
    BALANCE_CREDIT_QUERY,
    BALANCE_REMAINING_QUERY,
    START_DT,
    END_DT,
    EVENT_DT,
    B_COUNTRY_KEY,
    COUNTRY_KEY,
    CHARGE_NVAL,
    PHONE_NUM,
    B_PHONE_NUM,
    CELL_NVAL,
    IMEI_CVAL,
    SUBS_KEY,
    CALL_TYPE_KEY,
    DURATION_NVAL,
    DATA_VOL_NVAL,
    PRICE_PLAN_KEY,
    RECHARGE_VAL,
    BALANCE_NVAL,
    USAGE_TABLE,
    BALANCE_REMAINING_TABLE,
    BALANCE_TOPUP_TABLE,
    BALANCE_CREDIT_TABLE,
};