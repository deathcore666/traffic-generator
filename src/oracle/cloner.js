const EventEmitter = require('events');

const DataFetcher = require('../oracle/dataFetcher');
const params = {
    "user": "beeline_admin",
    "password": "beeline",
    "connectString": "192.168.0.166:32769/ORCLCDB.localdomain"
};

class DataCloner extends EventEmitter{
    constructor(day, inserter) {
        super();
        this.dataFetcher = new DataFetcher(params, day);
        this.dataInsert = inserter;
        this.dataStream = null;
        this.day = day;
    }

    async connect() {
        await this.dataFetcher.connect();
        this.dataStream = await this.dataFetcher.fetchUsageDataStream();
    }

    async releaseConnection() {
        if (this.dataStream)
            this.dataStream.end();

        await this.dataFetcher.releaseConnection();
    }

    clone() {
        this.dataStream
            .on('data', async (message) => {
                try {
                    await this.dataInsert.insertRecord(message);

                }
                catch (err) {

                }
            })
            .on('end', async () => {
                await this.releaseConnection();
                this.emit('end');
                console.log('end day:', this.day);
            })
            .on('error', async (err) => {
                console.log('error:', err);
                await this.releaseConnection();
            })
    }
}
module.exports = DataCloner;
