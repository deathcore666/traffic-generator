const {Transform} = require('stream');

class UsageDataTransformStream {
    constructor(map, day) {
        this.transformBalanceTopUpData = new Transform({
            readableObjectMode: true,
            writableObjectMode: true,

            transform(record, encoding, callback) {
                let resultMappedDataRecord = {};
                let msg = record;

                let startDate = new Date(msg[map['START_DT']]);
                startDate.setDate(startDate.getDate() + day);
                msg[map['START_DT']] = startDate;

                let endDate = new Date(msg[map['END_DT']]);
                endDate.setDate(endDate.getDate() + day);
                msg[map['END_DT']] = endDate;

                for (let i = 0; i < Object.keys(map).length; i++) {
                    resultMappedDataRecord[Object.keys(map)[i]] = msg[map[Object.keys(map)[i]]];
                }

                this.push(resultMappedDataRecord);
                callback();
            }
        });
    }

    get transformer() {
        return this.transformBalanceTopUpData;
    }
}

module.exports = UsageDataTransformStream;