const oracledb = require('oracledb');

class DataInsert {
    constructor(params, tableName) {
        this.connection = null;
        this.params = params;
        this.tableName = tableName;
    }

    async connect() {
        this.connection = await oracledb.getConnection(this.params);
    }

    async createQueryObject(dataMap) {
        let map;
        let bindsMap = {};

        try {
             map = await this.buildDataMap(this.tableName);
        }
        catch(err) {
            console.error('Error building columns index map: ' + err);
        }

        let resultQuery = 'INSERT INTO '+ this.tableName + ' VALUES (';
        for (let i = 0; i < Object.keys(map).length; i++) {
            resultQuery += ':' + Object.keys(map)[i] + ',';
        }
        resultQuery = resultQuery.slice(0, resultQuery.length - 1);
        resultQuery += ')';

        for (let i = 0; i < Object.keys(dataMap).length; i++) {
            bindsMap[Object.keys(dataMap)[i]] = {val: dataMap[Object.keys(dataMap)[i]]}
        }

        if (!DataInsert.compareKeys(dataMap, bindsMap))
            throw new Error ('Target columns do not match input data columns.');

        return {
            query: resultQuery,
            binds: bindsMap,
            options: {
                autoCommit: true,
            }
        }
    }

    static compareKeys (a, b) {
        let aKeys = Object.keys(a).sort();
        let bKeys = Object.keys(b).sort();
        return JSON.stringify(aKeys) === JSON.stringify(bKeys);
    }

    async insertRecord(dataMap) {
        let queryObject = await this.createQueryObject(dataMap);
        return await this.connection.execute(queryObject.query, queryObject.binds, queryObject.options);
    }

    async buildDataMap(tableName) {
        let i = 0;
        let resultMap = {};
        let query = 'SELECT * FROM ' + tableName;

        return new Promise(async (resolve, reject) => {
            try {
                let res = await this.connection.execute(
                    query,
                    [],
                    {maxRows: 1});

                res['metaData'].map(x => resultMap[x.name] = i++);
                resolve(resultMap);
            }
            catch (err) {
                let error = new Error('Error building data map. Error: ' + err);
                error.code = -1;
                reject(error)
            }
        })
    }

    releaseConnection() {
        const main = async () => {
            if (this.connection) {
                await this.connection.close();
            }
        };

        main()
            .catch(err => {
                console.log('Error closing inserter class connection ' + err);
            })
    }
}

module.exports = DataInsert;