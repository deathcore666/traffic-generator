const oracledb = require('oracledb');

const BEELINE = require('../const/beeline');
const UsageTransformStream = require('../oracle/usage-data-transform');


class DataFetcher {
    constructor(params, day) {
        this.connection = null;
        this.stream = null;
        this.connection = null;
        this.columnsMap = null;
        this.transformStream = null;
        this.params = params;
        this.day = day;
    }

    async connect() {
        this.connection = await oracledb.getConnection(this.params);
    }

    fetchUsageDataStream(query, params) {
        return new Promise(async (resolve, reject) => {
            if (!query)
                query = BEELINE.USAGE_QUERY;

            try {
                this.columnsMap = await this.buildDataMap(BEELINE.USAGE_QUERY);
                this.transformStream = new UsageTransformStream(this.columnsMap, this.day);

                this.stream = await this.fetchDataStream(query);
                let result = this.stream.pipe(this.transformStream.transformer);
                resolve(result)

            } catch (err) {
                this.releaseConnection();
                reject('Error at fetchUsageDataStream. Error: ' + err);
            }
        })
    }

    fetchDataStream(query, params = []) {
        return this.connection.queryStream(
            query,
            params,
            {fetchArraySize: 100});
    }

    async buildDataMap(query) {
        let i = 0;
        let resultMap = {};

        return new Promise(async (resolve, reject) => {
            try {
                let res = await this.connection.execute(
                    query,
                    [],
                    {maxRows: 1});

                res['metaData'].map(x => resultMap[x.name] = i++);

                resolve(resultMap);
            }

            catch (err) {
                let error = new Error('Error building data map. Error: ' + err);
                error.code = -1;
                reject(error)
            }
        })
    }

    releaseConnection() {
        const main = async () => {
            if (this.connection) {
                await this.connection.close();
            }
        };

        main()
            .catch(err => {
                console.log('Error closing connection fetcher:' + err);
            })
    }
}

module.exports = DataFetcher;